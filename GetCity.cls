	@AuraEnabled
	public static List < Service__c > getSeekerSearchResultCategory(String zip) {
    	Result result = (Result) JSON.deserialize(getAddressFromZip(zip), Result.class);
			if (result != null && result.results.size() > 0 && result.results[0].address_components.size() > 0) {
				for (AddressComponents adc: result.results[0].address_components) {
					if (adc.types.contains('locality')) {
						citySet.add(adc.long_name);
					}
				}
			}
    }
    @AuraEnabled(cacheable = true)
	public static string getAddressFromZip(String zip) {
		String result = null;
		try {
			String api = Label.GoogleAPIKey;
			String apiURL = 'https://maps.googleapis.com/maps/api/geocode/json?address=' + zip + '&key=' + api; // + Label.GoogleAPIKey;
			//system.debug('apiURL is1 ' + apiURL);
			HttpResponse res = GoogleAPI.getResponse(apiURL);
			if (res.getStatusCode() == 200) {
				//system.debug('API invoked successfully');
				result = res.getBody();
			}
		} catch (Exception e) {
			throw new AuraHandledException('Error while getting current location ---> ' + e.getMessage());
		}
		return result;
	}